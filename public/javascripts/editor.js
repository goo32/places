var pl = (function($){

  var features = [],
      drawnFeatures,
      editTool;
  
  var _pl = {

    init: function(layerGroup, editHandler) {
      drawnFeatures = layerGroup;
      editTool = editHandler;
    },

    onLocationFound: function(e) {
      console.log("Location found");
    },

    onLocationError: function(e) {
      alert(e.message);
    },

    onDrawCreated: function (e) {
      console.log("draw:created");
    
      var type = e.layerType,
          layer = e.layer;
    
      drawnFeatures.addLayer(layer);
      var id = drawnFeatures.getLayerId(layer);
    
      var feature = {
        id: id,
        layer: layer,
        name: type + ' ' + id
      }
    
      features[id] = feature;
    
      layer.on('click', _pl.onLayerClick);

      addFormForNewFeature(feature);
    
      // enables edit mode
      editTool.enable();
      //expands the accordian item just added
      $('#heading' + feature.id + ' a').click();
    },

    onLayerClick: function(e) {
      console.log("Layer clicked");
      
      var layer = e.target;
      var id = drawnFeatures.getLayerId(layer);
      //expands the accordian item for this layer
      $('#heading' + id + ' a').click();
    },
    
    onDrawEdited: function(e) {
      console.log("draw:edited");

      var layers = e.layers;
      layers.eachLayer(function (layer) {
        console.log("Edited layer:");
        console.log(layer);          
      });      
    },

    onDrawEditStop: function(e) {      
      console.log('edit:stop');
      $('#overlay').slideToggle('fast');
    },
    
    onDrawEditStart: function(e) {
      console.log('edit:start');
      $('#overlay').slideToggle('fast');
    },

    onDrawDeleteStart: function(e) {
      console.log('delete:start');
    },

    onDrawDeleted: function(e) {
      console.log("draw:deleted");
      var layers = e.layers;
      layers.eachLayer(function (layer) {
        var id = drawnFeatures.getLayerId(layer);
        $('#heading' + id).parent('div').remove();
      });      
    },

    onDrawDeleteStop: function(e) {
      console.log('delete:stop');
    }
  }

  function addFormForNewFeature(feature) {
    // Render form
    $('#overlay #accordion').append(
      _renderItem(feature, _renderForm(feature) + '<a href="#" class="remove-layer" id="' + feature.id + '">remove</a>' )
    );

    // Setup 'remove' link
    $('.remove-layer#' + feature.id).on('click', function() {
      console.log("Remove marker");
      var id = $(this).attr('id');
      drawnFeatures.removeLayer(features[id].layer);
      $(this).closest('div.panel-default').remove();  
    });

    // Setup the pan to behaviour when the form is opened
    $('#collapse' + feature.id).on('show.bs.collapse', function () {
      console.log("Show panel");
      var id = $(this).data('marker-id');
      var layer = features[id].layer;
  
      if(typeof layer.getBounds === 'function') 
        map.panTo(layer.getBounds().getCenter());
      else {
        map.panTo(layer.getLatLng());
      }
    });
  }

  function _renderForm(item) {
    return '<form id="form' + item.id + '" class="well">'+
            '<label><strong>Number:</strong></label><br/>'+
            '<input type="text" class="span3" placeholder="Required" id="number" name="number" /><br/>'+
            '<label><strong>Name:</strong></label><br/>'+
            '<input type="text" class="span3" placeholder="Required" id="name" name="name" /><br/>'+
            '<label><strong>Description:</strong></label><br/>'+
            '<input type="text" class="span3" placeholder="Optional" id="description" name="description" />'+
          '</form>';
  }

  function _renderItem(item, content) {
    return '<div class="panel panel-default">'+
      '<div class="panel-heading" role="tab" id="heading' + item.id + '">'+
        '<h4 class="panel-title">'+
          '<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse' + item.id + '" aria-expanded="false" aria-controls="collapse' + item.id + '">'+
            item.name +
          '</a>'+
        '</h4>'+
      '</div>'+
      '<div id="collapse' + item.id + '" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading' + item.id + '" aria-expanded="false" data-marker-id="' + item.id + '">'+
        '<div class="panel-body">'+ 
          content +
        '</div>'+
      '</div>'+
    '</div>';
  }

  return _pl;
})(jQuery);
