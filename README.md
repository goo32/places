# Places - Editor

Proof of concept for adding features to a map and labeling them with a simple form.


## Up and running

Install dependencies

* install node/npm
* run `>npm install`

Run the express server:

```
>npm start
```

Open the map in the browser at http://localhost:3000


## Using the map

* Add a few features to the map using the toolbar at the top left of the screen.
* Once a feature is placed on the map a form overlay will pop-up on the left. Fill out the forms to describe the features.
* When in 'edit mode'

  * clicking on a feature will open up it's corresponding form.
  * clicking on a form link will centre the map on the corresponding feature.


## TODO

* The form overlay won't scale to a large number of features so it should probably go into a scrolling sidebar
* Persist local edits to local storage so they're not lost on a refresh
* Persist features to a backend on save
* Use feature names in form link instead of shape names